﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Import
{
    public class AssetImport : MonoBehaviour
    {
        public Image image;
        public Material material;
        public Texture2D[] textures;
        public Texture2D atlas;
        public Rect[] rects;
        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(LoadGif());
            //StartCoroutine(LoadImage());
        }

        IEnumerator LoadImage()
        {
            string filepath = "C:/Users/Jonas Hingeberg Ruge/Downloads/5E D&D Storm King's Thunder 2020-03-03/modules/DD Storm Kings Thunder/Maelstrom-DM-L1.jpg";
            UnityWebRequest unityWebRequest = UnityWebRequestTexture.GetTexture(filepath);
            yield return unityWebRequest.SendWebRequest();

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.LogError(unityWebRequest.error);
            }
            else
            {
                Texture texture = ((DownloadHandlerTexture)unityWebRequest.downloadHandler).texture;
                material.mainTexture = texture;
            }
        }

        IEnumerator LoadGif()
        {
            string filepath = "C:/Users/Jonas Hingeberg Ruge/Downloads/safe_image.gif";
            UnityWebRequest unityWebRequest = UnityWebRequest.Get(filepath);
            yield return unityWebRequest.SendWebRequest();

            int count = 0;
            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.LogError(unityWebRequest.error);
            }
            else
            {
                byte[] bytes = unityWebRequest.downloadHandler.data;
                
                yield return StartCoroutine(UniGif.GetTextureListCoroutine(bytes, (gifTextureList, loopCount, width, height) =>
                {
                    for (int i = 0; i < gifTextureList.Count; i++)
                    {
                        textures[i] = gifTextureList[i].m_texture2d;
                        count++;
                    }
                }));

            }
            Debug.Log(count);
            atlas = new Texture2D(8192, 8192);
            rects = atlas.PackTextures(textures, 0, 8192);
            material.mainTexture = atlas;
            material.mainTextureScale = new Vector2(rects[0].width, rects[0].height);
        }

        // Update is called once per frame
        private int frames = 0;
        private int currentGifFrame = 0;
        void Update()
        {
            frames++;
            if(frames%10 == 0)
            {
                frames = 0;
                if (rects.Length > 0)
                {
                    material.mainTextureOffset = new Vector2(rects[currentGifFrame].x, rects[currentGifFrame].y);
                    currentGifFrame++;
                    if(currentGifFrame>=rects.Length)
                    {
                        currentGifFrame = 0;
                    }
                }
            }
        }
    }
}
