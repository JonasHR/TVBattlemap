# TVBattlemap

This is a piece of software that enhances the magic of DND by having an easy way to present your digital battlemaps to your players.
It is developed solvely to be used at in person DND games, on a tv screen, for me and the sessions that I hold. If you find it useful feel free to grap a copy.

Images say more than a thousand words. Gif's even more so:

INSERT IMAGES AND GIFS HERE

Note that this piece of software have an opensource MIT license and always will have.

Current features:
- None at the moment, it is very early in the development cycle

Planned features, in no perticular order (note any features listed here can change at any moment, without notice, if I feel like it):
- Support for image import (including editing)
- Support for gif import (including editing)
- Support for mp4 import (or other video format, whatever I think fits)
- Day/night cycle
- Import from VTT's and other digital battlemaps
- Dynamic light and FOV system
- Saving and loading of maps (both local and online hopefully)
- Lighting based on movement of gif/mp4
- QOL makers
- Support for Chromecast (or other device)
- Easy to install steam version

MIT License

Copyright (c) 2020 Jonas Hingeberg Ruge

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
